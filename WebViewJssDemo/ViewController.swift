//
//  ViewController.swift
//  WebViewJssDemo
//
//  Created by user on 06/08/20.
//  Copyright © 2020 aulakh. All rights reserved.
//

import UIKit
import WebKit

class ViewController: UIViewController {
    
    @IBOutlet weak var wbView: WKWebView!
    
     override func viewDidLoad() {
            super.viewDidLoad()
            let bundleURL = Bundle.main.resourceURL!.absoluteURL
            let html = bundleURL.appendingPathComponent("try.html")
            wbView.loadFileURL(html, allowingReadAccessTo:bundleURL)
        }
            
    @IBAction func nativeBtnClicked(_ sender: Any) {
        let js = "hideText()"
        wbView.evaluateJavaScript(js, completionHandler: nil)
    }
    
    }
